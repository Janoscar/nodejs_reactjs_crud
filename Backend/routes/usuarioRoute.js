'use strict'
 
var express = require('express');
 
var userController = require('../controllers/usuarioController');

var api = express.Router();
 
api.post('/usuario', userController.guardar);
api.get('/usuario', userController.listarTodos);
api.post('/usuario/validar', userController.validar);
api.delete('/usuario/:id', userController.eliminar);
api.put('/usuario/:id', userController.modificar);

module.exports = api;